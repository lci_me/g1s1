# Devoir blanc

1.	select **NomC** from* chercheur, AFF* where *chercheur*.**codeC=*AFF*.**codeC** and **codeP='p5'**;

2.	select **nomP** from *projet* where **codeP** not in(select **codep** from aff where **codeC='ch1'** or **codeC='ch3'**);

3.	select distinct **nomC** 
from *chercheur,projet, AFF*
where *aff*.**codeC**=*chercheur*.**codeC** and *aff*.**codeP**=*projet*.**CodeP** and **nomP='IMAGE'** and 
**aff.Codec** not in 
(select **codec** from *AFF, projet* where *aff*.**codeP=projet.CodeP** and **nomP!='IMAGE**);

4.	select **codec**, count(`*`) as **nb_projet** from *aff* group by **codeC**;

5.	select **nomC** from *chercheur, AFF* where *aff*.**codeC**=*chercheur*.**codeC** group by *AFF*.**codeC**, **nomC** having count(`*`)=(select max(count(`*`))from *AFF* group by *codeC*);

6.	create view **ProjetChercheur1** as
(select **codeP** from *aff* where **codeC='ch1'**);
select **codeC**  from ( select `*` from *aff*, *ProjetChercheur1* where *aff*.**codeP** = *ProjetChercheur1*.**codeP**) T
group by **codeC**
having count(`*`) = (select count(`*`) from *ProjetChercheur1*);

7.	select **nomE, nomp** as **moy** from *projet, equipe* where *equipe*.**codeE**=*projet*.**codeE** and *projet*.**budgetP**>ALL(select avg(**budgetP**) from *projet*);

8.	select **nomE** from *equipe, chercheur* where *equipe*.**codeE**=*chercheur*.**codeE** GROUP BY *equipe*.**nomE** HAVING COUNT (*chercheur*.**CodeE**)>= 3;
