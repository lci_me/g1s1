# Variable

## Généralités :
•	Il est possible de créer et d’affecter une valeur à une variable dans un Shell  
•	Une variable est désignée par son nom précédé du caractère **‘$’**   
•	Une variable Shell n’est pas typée : son contenu est une chaîne de caractères totalement quelconque  

## Affectation :
•	L’affectation d’une variable change selon le type de Shell  
•	En bas : la commande **‘=’** permet d’affecter une variable. Lors de l’affectation, **le signe $ est absent**  *(ex : test = valeur)*  
## Affichage : 
•	Echo permet de visualiser une variable (ex : echo $test)  
•	Une variable est locale au Shell où elle a été affectée   
•	Sur certains Shells, une variable non déclarée a une valeur nulle  
### Exemple : 
•	Créez une variable test dont la valeur est « riendutout’  
•	Testez les commandes : *echo $test, echo A$test, echo A$testA, echo A${test}A*  
## Une variable peut être utilisée dans une commande : 
•	Créez une variable « repertoire » dont la valeur est « /bin  
•	Utilisez-la avec la commande ls  
### Exemple : Testez les commandes suivantes
•	A = 10 ; B=A ; echo $B -> A  
•	B = $A ; echo $B -> 10  
•	A = 3 ; echo $B -> 10  
## Il est possible de récupérer le résultat d’une commande dans une variable avec le caractère ` : 
### Exemple : Testez les commandes suivantes 
•	datedujour = `date` ; echo $datedujour -> 05/12/2018  
•	datedujour = la date du jour est `date` ‘; echo $datedujour -> erreur  
•	datedujour = ‘la date du jour est `date` ‘ ; echo $datedujour -> la date du jour est `date`  
•	datedujour = « la date du jour est `date` » ; echo $datedujour -> la date du jour est 05/12/2018 
> Les "date" qui sont rouge sont entrourées du symbole `

## Variable définies : 
•	Il est possible de voir la liste des variables actuellement définies grâce à la commande « set »  
•	Pour détruire une variable, il faut utiliser la commande unset  
 
## Variables automatiques = variables prédéfinies par le système
•	**$PATH** donne le chemin d’accès par défaut aux exécutables  
•	**$_** : la dernière commande  
•	**$PWD** : le chemin absolu du répertoire courant  
## Affectation interactive : 
La commande read permet de lire sur l’entrée standard la valeur à affecter à une variable *(ex : read a)*  
On peut affecter plusieurs variables à la fois *(ex : read b c d)*   

## Variables automatiques = variables prédéfinies par le système
•	$PATH donne le chemin d’accès par défaut aux exécutables  
•	$_ : la dernière commande  
•	$PWD : le chemin absolu du répertoire courant  
### Affectation interactive : 
La commande read permet de lire sur l’entrée standard la valeur à affecter à une variable (ex : read a)   
On peut affecter plusieurs variables à la fois (ex : read b c d)  
### Une commande exécutable, cript ou autre, accepte des arguments : 
Pour un script, les arguments sont accessibles sous la forme de variables nommées $0, $1, $2  
$0 correspond à la commande elle-même, $1 au premier argument, $2 au deuxième…  
S’il y a plus de 9 arguments, il faut utiliser la segmentation avec les accolades : ${10}   
