# Commande

**Cat** affiche le contenu du fichier  
**Stat** affiche les caractéristiques d’un fichier  
**Ls** affiche les caractéristiques d’une liste de fichier  
**Rm** nom fichier supprimer un fichier  
**Touch** permet de modifier les caractéristiques de date d’un fichier, voire de créer un fichier vide  
**Cp** copier un fichier  
**Mv** déplacer un fichier  
**More** alternative pour visualiser le contenu d’un fichier  
## Répertoires : 
**Pwd** donne le chemin absolu du répertoire courant  
**Cd** permet de changer le répertoire courant  
**Ls** permet d’obtenir la liste des fichiers contenus dans un répertoire  
**Mkdir** créer un répertoire   
**Alias** créer un alias   

## Le système de gestion de fichiers :
### Fichier spéciaux :
#### Un fichier spécial représente généralement un périphérique :
* Un Périphérique est un matériel physique connecté à l’unité centrale : disque ; souris, réseau…
* Sous linux, périphérique est présent dans l’arborescence, sous le répertoire \dev
* Un pilote est une fonction du système permettant d’exploiter le périphérique vis des appels systèmes

### Un périphérique peut être virtuel :
* Est géré comme un périphérique « normal » mais ne correspond à aucune réalité physique
*Exemple : Ecran virtuels, partition logiques, poubelle…

## Fichier liens : deux types
### Un lien physique est l’entité qui relie un nom de fichier à l’inode correspondant
* Plusieurs liens physiques pour le même inode = une même donnée est repérée par plusieurs noms de fichiers différant
* N’a pas d’équivalent sous Windows
### Un lien symbolique est un fichier qui ne contient que le chemin et le nom d’un autre fichier
* L’inode pointé par le lien symbolique renvoie sur un nom de fichier au lieu d’un bloc de données 
* En langage Windows ; un lien symbolique est un raccourci
* En manipulant le lien, on manipule en fait le fichier dont le chemin est stocké dans le lien 
#### Commande associée : ln
* Sans option : lien physique
* Avec option –s : lien symbolique
### Tubes :
Un tube est un fichier permettant à deux processus locaux de communiquer  
Dans une commande, un tube permet de rediriger la sortie standard d’une première commande ver l’entrée standards d’une deuxième  
* Format : commande1 | commande2
* Exemple Essayer de rediriger le sorite standard de « ls » ver l’entrée standard de « more »

### Sockets :
Un socket est un tube entre deux processus distants  
Caractérisée par un numéro de port et une adresse IP  
Plus de détails en programmation système :  
