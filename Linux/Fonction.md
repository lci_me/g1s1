# Fonction

Le shell permet la declaration de fonction qui pourront être appelées au seins d'un script:  
<pre><code>
fonction{
Commande1
Commande2
}
</code></pre>
 L'appel d'une fonction est plus rapide que l'appel d'un script   
 L'instruction "Return n" permet de quitter la fonction en spécifant le statut n   
 
 