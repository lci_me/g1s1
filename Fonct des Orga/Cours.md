# Organisation est leurs contextes

## Qu'est ce qu'une organisation

Une organisation est une **action collective a la poursuite d'une realisation commune**  l'action collective est un systeme,
de l'autorité/hierarchie et un regroupement informel de personne : **ensemble finalisé d'ensemble et d'interaction**   

Une organisation peut aussi etre **un ensemble finalisé d'element en interaction** qui mette en oeuvre des actions pour atteindre un objectif. Une tendance tournée 
vers *le fonctionnement des systemes* et *l'etudes des rapports humains*   

Pour resumer les deux ensembles:
* Objectifs a atteindre
* de structures 
* d'interaction
* de techniques
* d'une culture 

Definition: **L'organisation suppose une repartition des taches, une stabilité, et une coordination**  

Deux types d'organisation:  
* Les organisations marchandes
* Les organisations non-marchandes 

## Qu'est ce qu'une entreprise

Definition **Une unité organisationnelle de production de biens et de services jouissant d'une certaine autonomie de décision**  

Classification:  
* Juridique
* Taille 
* Secteurs
* En fonctions de la concurence 
* Des marchés 
* Des composantes externes 

Micro environnement: **Il est composé des acteurs proches, voire au contact de l'entreprise. Ils forment la majeure partie des éléments**  
Macro environnement: **Des variables externes a priori plus éloignées, mais qui peuvent avoir une incidence positive ou négative sur le business de votre affaire.**  

## La stratégie 

La stratégie peut etre *réactive* (action décidée) ou *proactive* (action autonome)  
La stratégie a pour but: 
* Faire face a la concurrence 
* Définir un perimetre d'activité 
* reagir a l'évolution de l'environnement
* 



