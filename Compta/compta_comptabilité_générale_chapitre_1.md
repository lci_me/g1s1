# chapitre 1: les principes comptables

## comptabilisation en partie double
### le compte
*(cf page 7)*
$$Solde = \mid Débit - Crédit\mid$$ 
si $$Débit > Crédit$$ alors solde débiteur (bien)
si $$Crédit > Débit$$ alors solde créditeur (pas bien)

### le principe de la partie double
2 questions à ce poser:
- dequels comptes sort le flux ou vient la ressource ? Dans le **compte de départ**, mettre la somme à **droite** dans la colonne Crédit-Ressource-Sortie
- dans quel(s) autre(s) comptes est employé/entré le flux ou **à quoi** est-il employé ? dans le **compte d'arrivé**, mettre la somme à **gauche** dans la colonne Débit-Emploi-Entrée