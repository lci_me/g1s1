# Intro
**entreprise**: unité qui produit des biens et services pour les vendres

## Unité au cœur de multiples flux économiques
- **flux réels**: mouvements de biens et services entres agents économiques
- **flux financiers**: mouvements d'argent entres agents économiques
- **marchandise**: bien ou service vendu en l'état d'acquisition
- **matière première**: bien ou service à transformer avant revente

### schéma
*(transcription schéma page 2)*

**Organisme**
- ent doit: cotisations sociales
- ent reçoit: couverture de risques

**Salarié**
- ent doit: salaires
- ent reçoit: travail

**Clients**
- ent doit: bien et services
- ent reçoit: paiement, engagement de paiement

**Fournisseur**
- ent doit: paiement, engagement de paiement
- ent reçoit: matière première, marchandises

**Propriétaire**
- ent doit: dividende, titre de propriété
- ent reçoit: apport en capital

**Banque**
- ent doit: remboursement avec intérêts
- ent reçoit: apport de capital

## comptabilité générale
### définition
Un système d'information économique et financier reglementé donnant une représentation synthétique de l'état financier de l'entreprise à travers 2 documents de synthèse (bilan ou état du patrimoine + compte de résultat).

### Pour quels destinataires ?
- des utilisateurs internes: dirigeant, direction financière, salariés
- des utilisateurs externes: fisc, tiers

### Pourquoi ?
- un outil pour connaitre l'état de l'entreprise, décider, répondre à des obligations légales et communiquer

### obligations comptables
*(cf page 3)*

### principes généraux du droit comptable
*(cf page 3)*


## Cycle de vie de l'entreprise, vocabulaire de base et problématique comptable
À sa création, pour fonctionner, l'entreprise a besoin de ressources qu'elle emploie et fait face à un cycle d'exploitation.
### ressources de l'entreprise
- capital: apport réalisé par les propriétaires de l'entreprise (individuel si apport exploitant, social si apport d'associé ou d'actionaires)
- emprunt: auprès des banques
- fournisseurs: dans les usages, délai de paiement très souvent octroyé par le fournisseur lors d'un achat (ressource transitoire étant donné qu'aucune somme n'est alors à débourser au moment de l'échange des marchandises) = une dette sans intérêt

### moyens
- l'outil de travail utilisé durablement = des **immobilisations**
- des biens destinés à être revendus en l'état ) = des **marchandises**/capital circulant
- des biens changeant en nature = des **matières premières**, composants.../capital circulant

### cycle d'exploitation
*(cf schéma page 4)*


## une entreprise doit réaliser des bénéfices
$$Résultats = Produit - Charge$$
Bénéfice si résultats positifs et Pertes sinon


## méthode
**principe de la partie double**
Pour une opération mettre 2 fois cette somme:
- à droite d'un compte
- à gauche d'un autre compte