# EX sur les caracteres 

## Calculer le nombre d’occurences d’un caractère :

    def compte_car(s,c):
    i=0
    n=0
    while(i<len(s)):
        if(s[i] == c):
            n=n+1
        i=1+i
    return n 
    
## Déterminer l’indice minimum (le plus à gauche) d’un caractère :

    def compte_car(s,c):
    i=0
    n=0
    while(i<len(s)):
        if(s[i] == c):
            return i
        i=1+i